def my_to_s(arg)
  arg.to_s
end

def my_round(num)
  num.round
end

def my_modulo(dividend, divisor)
  dividend%divisor
end

def my_lcm(int_one, int_two)
  int_one.lcm(int_two)
end

def to_stringified_float(int)
  int.to_f.to_s
end

def absolute_sum(num_one, num_two)
  num_one.abs + num_two.abs
end

def negative(num)
  num.abs - (num.abs*2)
end

def last_digit(int)
  int%10
end

def last_digit_odd?(int)
  last_digit(int).odd?
end

def gcd_of_last_digits(int_one, int_two)
  last_digit(int_one).gcd(last_digit(int_two))
end

def last_n_digits(num, n)
  num.to_s.reverse[0..n-1].reverse.to_i
end

def dec_remainder_of_two_floats(f_dividend, f_divisor)
  ((f_dividend/f_divisor)%1.0).round(1)
end

def dec_remainder_of_two_integers(i_dividend, i_divisor)
  dec_remainder_of_two_floats(i_dividend.to_f, i_divisor.to_f)
end

# This works and doesn't use modulo, but fails at line 144 of the
# spec file. Not sure why, or what 'a' does in the spec file.

def int_remainder_without_modulo(i_dividend, i_divisor)
  i_dividend - ((i_dividend/i_divisor)*i_divisor)
end
